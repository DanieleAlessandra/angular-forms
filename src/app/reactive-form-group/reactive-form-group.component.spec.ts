import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormGroupComponent } from './reactive-form-group.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('ReactiveFormGroupComponent', () => {
  let component: ReactiveFormGroupComponent;
  let fixture: ComponentFixture<ReactiveFormGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveFormGroupComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveFormGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
