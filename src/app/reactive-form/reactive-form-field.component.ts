import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-reactive-form-field',
  templateUrl: './reactive-form-field.component.html',
  styleUrls: ['./reactive-form-field.component.scss']
})
export class ReactiveFormFieldComponent implements OnInit {
  name = new FormControl('');

  constructor() { }

  ngOnInit() {
  }

  updateName() {
    this.name.setValue('Nancy');
  }

}
