import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {TemplateFavoriteColorComponent} from './template-favorite-color.component';
import {FormsModule} from '@angular/forms';

describe('TemplateFavoriteColorComponent', () => {
  let component: TemplateFavoriteColorComponent;
  let fixture: ComponentFixture<TemplateFavoriteColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateFavoriteColorComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateFavoriteColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the favorite color in the component', fakeAsync(() => {
    const input = fixture.nativeElement.querySelector('input');

    const event = new Event('input');

    input.value = 'Red';

    /// Qui è ancora vuoto
    expect(component.favoriteColor).toEqual('');

    input.dispatchEvent(event);

    fixture.detectChanges();

    /// Qui è cambiato
    expect(component.favoriteColor).toEqual('Red');
  }));

  it('should update the favorite color on the input field', fakeAsync(() => {
    component.favoriteColor = 'Blue';

    const input = fixture.nativeElement.querySelector('input');


    fixture.detectChanges();

    /// Qui è ancora vuoto
    expect(input.value).toBe('');

    tick();

    /// Qui è cambiato
    expect(input.value).toBe('Blue');

  }));
});
