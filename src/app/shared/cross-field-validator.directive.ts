import { Directive } from '@angular/core';
import {AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';

/// This is for Template
@Directive({
  selector: '[appIdentityRevealed]',
  providers: [{ provide: NG_VALIDATORS, useExisting: IdentityRevealedValidatorDirective, multi: true }]
})
export class IdentityRevealedValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors {
    const alterEgo = control.get('alterEgo');
    const name = control.get('name');
    return alterEgo && name && alterEgo.value === name.value ? { identityRevealedValidator: true } : null;
  }
}

/// This is for Reactive
export const firstAndLastNameValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const firstName = control.get('firstName');
  const lastName = control.get('lastName');

  return firstName && lastName && firstName.value === lastName.value ? { firstAndLastNameValidator: true } : null;
};
