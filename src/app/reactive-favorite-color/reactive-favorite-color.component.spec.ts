import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReactiveFavoriteColorComponent} from './reactive-favorite-color.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('ReactiveFavoriteColorComponent', () => {
  let component: ReactiveFavoriteColorComponent;
  let fixture: ComponentFixture<ReactiveFavoriteColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReactiveFavoriteColorComponent],
      imports: [ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveFavoriteColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the value of the input field', () => {
    const input = fixture.nativeElement.querySelector('input');

    const event = new Event('input');

    input.value = 'Red';

    /// Qui è ancora vuoto
    expect(fixture.componentInstance.favoriteColorControl.value).toEqual('');

    input.dispatchEvent(event);

    /// Qui è cambiato
    expect(fixture.componentInstance.favoriteColorControl.value).toEqual('Red');
  });

  it('should update the value in the control', () => {
    component.favoriteColorControl.setValue('Blue');

    const input = fixture.nativeElement.querySelector('input');

    /// Qui è cambiato immediatamente
    expect(input.value).toBe('Blue');
  });
});
