import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ReactiveFavoriteColorComponent} from './reactive-favorite-color/reactive-favorite-color.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TemplateFavoriteColorComponent} from './template-favorite-color/template-favorite-color.component';
import {ReactiveFormFieldComponent} from './reactive-form/reactive-form-field.component';
import {ReactiveFormGroupComponent} from './reactive-form-group/reactive-form-group.component';
import {TemplateFormComponent} from './template-form/template-form.component';
import {ForbiddenValidatorDirective} from './shared/forbidden-name-validator.directive';
import {IdentityRevealedValidatorDirective} from './shared/cross-field-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    ReactiveFavoriteColorComponent,
    TemplateFavoriteColorComponent,
    ReactiveFormFieldComponent,
    ReactiveFormGroupComponent,
    TemplateFormComponent,
    ForbiddenValidatorDirective,
    IdentityRevealedValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule, // This is for Reactive Forms
    FormsModule, // THis is for Template Driven Forms
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
