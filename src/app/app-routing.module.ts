import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReactiveFavoriteColorComponent} from './reactive-favorite-color/reactive-favorite-color.component';
import {TemplateFavoriteColorComponent} from './template-favorite-color/template-favorite-color.component';
import {ReactiveFormFieldComponent} from './reactive-form/reactive-form-field.component';
import {ReactiveFormGroupComponent} from './reactive-form-group/reactive-form-group.component';
import {TemplateFormComponent} from './template-form/template-form.component';

const routes: Routes = [
  {
    path: 'reactive-1', component: ReactiveFavoriteColorComponent
  },
  {
    path: 'reactive-2', component: ReactiveFormFieldComponent
  },
  {
    path: 'reactive-3', component: ReactiveFormGroupComponent
  },
  {
    path: 'template-1', component: TemplateFavoriteColorComponent
  },
  {
    path: 'template-2', component: TemplateFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
